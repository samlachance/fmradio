require 'rubygems'
require 'bundler'
require 'slim'
require 'sass/plugin/rack'
require 'json'

Sass::Plugin.options[:style] = :compressed
use Sass::Plugin::Rack

Bundler.require

Dir['./lib/*.rb'].each { |file| require file }

require './app'

run Sinatra::Application
