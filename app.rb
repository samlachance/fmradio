require 'sinatra'

get '/' do
  @stations = stations
  slim :index
end

post '/radio' do
  case params['action']
  when 'play'
    kill_player
    play params['stream']
  when 'stop'
    kill_player
  end

  redirect '/'
end

private

def kill_player
  `killall mplayer`
end

def play(path)
  flags = []

  if path.split('.').last == 'm3u'
    flags << '-playlist'
  end

  `mplayer #{flags.join (' ')} #{path} </dev/null >/dev/null 2>&1 &`
end

def stations
  @@stations ||= Station.seed
end

