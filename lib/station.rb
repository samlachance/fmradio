class Station
  attr_reader :stream, :name

  def initialize(**args)
    @name = args[:name] 
    @stream = args[:stream]
  end

  def self.seed
    raw_stations = JSON.parse(File.read('config/stations.json')).to_h
    stations = []

    raw_stations['stations'].each do |station|
      stations << self.new(name: station['name'], stream: station['stream'])
    end

    stations
  end
end